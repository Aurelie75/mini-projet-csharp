# Mini projet de Csharp

Ce projet qui a pour but de valider les acquis en conception objet, versionning,UML, T.U, .NET, C# et Winforms


## Objectif ?

Réalisation d'un gestionnaire de taches basique sous la forme d'une application Winforms.
L'application doit permettre l'ajout de tâche, l'édition et la suppression(indiquer que la tache est accomplie).
Nous avons ajouté en plus une fonction permettant de lister les tâches déjà présentes ainsi que de compter le nombre de tâches à accomplir.


## Composition ?

- Une bibliothèque de classes C# 
- Une application Winforms en VB.NET 
- Les tests unitaires associés 

## Diagramme UML 

 ![UML](/UML.png)

## Auteur

> [Aurélie PLET]
> [Estelle Castetnau]
