﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TaskManagerLib;

namespace BasicTaskManager
{
    public partial class Form1 : Form
    {
        List<Tache> list_taches;
        string path = @"todolist.txt";
        int pid;


        public Form1()
        {
            InitializeComponent();
            Fichier.ouverture_fichier(path);
            pid = 1;
            list_taches = Fichier.create_List(path);
            // affichage_list();
        }

        public void reenitialisation() // efface le contenu actuel des champs
        {
            textBoxDesc.Text = "";
            textBoxNom.Text = "";
        }

        private void affichage_list()
        {
            textBox1.Clear();
            foreach (Tache task in list_taches)
            {
                textBox1.Text = string.Format("{0} N° : {1} \r\n Name :  {2} \r\n Description: {3} \r\n DeadLine: {4}\r\n****************************************** \r\n", textBox1.Text, task.get_Id(), task.get_Name(), task.get_Desc(), task.get_Deadline());
            }
        }

        private void Ajouter_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxNom.Text) | string.IsNullOrEmpty(textBoxDesc.Text) | string.IsNullOrEmpty(dateTime.Text)) // on verifie que tout les champs sont remplis
            {
                MessageBox.Show("Erreur, veuillez remplir tous les champs", "Erreur");
                return;
            }
            else
            {

                pid = Fichier.Calcul_id(list_taches, pid);
                Tache tache;

                tache = new Tache(pid, textBoxNom.Text, textBoxDesc.Text, dateTime.Value.Date);
                list_taches.Add(tache);
                MessageBox.Show(string.Format("Tache n°{0} ajoutée (Youpi une de plus) .", pid), "Ajout");
                reenitialisation();
                Fichier.ecriture_fichier(list_taches, path);
                affichage_list();
            }

        }



        private void Afficher_Click(object sender, EventArgs e)
        {
            affichage_list();
        }

        private void supprimer_Click(object sender, EventArgs e)
        {
            if (textBoxNom.Text == "") // on verifie que l'utilisateur rempli le nom
            {
                MessageBox.Show("Il faut indiquer le nom de la tache a supprimer", "Erreur");
            }
            else
            {                
                string tacheS = textBoxNom.Text;
                int i;
                for (i = 0; i < list_taches.Count; i++) //On parcours la liste et une fois qu'on tombe sur le bon nom, on supprime l'objet Tache de la liste
                {
                    if (list_taches[i].get_Name() == tacheS)
                    {
                        list_taches.RemoveAt(i);
                        MessageBox.Show(string.Format("La tâche {0} est accomplie", tacheS), "Suppression");
                    }
                }
                Fichier.ecriture_fichier(list_taches, path);
                affichage_list();

            }
        }

        private void BCompteur_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show(string.Format("Vous avez {0} tache(s) à accomplir ! Enjoy :)",Fichier.compteur(list_taches)),"Nombre de tâche(s)");
        }

        private int modifiertache(string Name)
        {
            foreach (Tache task in list_taches) //  On recherche une tache qui a le même nom que celui rempli par l'utilisateur
            {
                if (Name == task.get_Name())
                {
                   
                    if (!string.IsNullOrEmpty(textBoxDesc.Text))
                    {
                        task.set_Desc(textBoxDesc.Text); // changement de la description
                    }
                    if (dateTime.Value==task.get_Deadline())
                    {
                        task.set_Deadline(dateTime.Value); // changement de l'heure
                    }

                    return 1;
                }
                else
                {
                    MessageBox.Show("Oups! nous n'avons trouvé aucune tache correspondante ", "Erreur");
                    return 0;
                }
            }
            return 0;
        }

        private void BEditer_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxNom.Text) & string.IsNullOrEmpty(textBoxDesc.Text)) // on verifie que l'utilisateur a bien rentré un nom afin de pouvoir le retrouver par la suite
            {
                MessageBox.Show("Veuillez préciser le nom de la tache à modifier ", "Erreur");
                return;
            }
            string Name = textBoxNom.Text;
            if (modifiertache(Name) == 1) // on verifie que on a trouvé une tache qui correspond au nom avant de faire une modification
            {
                MessageBox.Show(string.Format("La tâche {0} a bien été edité.", Name), "Modification réussie");
                reenitialisation();
                Fichier.ecriture_fichier(list_taches, path);
                affichage_list();
            }
            else
            {
                MessageBox.Show("La modification a échouée veuillez réessayer ", "Erreur");
                return;
            }
        }


    }


}
