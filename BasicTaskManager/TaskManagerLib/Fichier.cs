﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TaskManagerLib;

namespace BasicTaskManager
{
        public class Fichier
        {
        public static void ouverture_fichier(string path)
        {
            // création du fichier si inexistant
            if (!File.Exists(path))
            {
                File.WriteAllText(path, "");
            }
        }
        
        
        public static List<Tache> create_List(string path) 
        {
            
            List<Tache> list_taches = new List<Tache>();
            string[] lines = File.ReadAllLines(path);
            foreach (string line in lines)
            {

                string[] ligne = line.Split(';');
                int id;
                bool success = Int32.TryParse(ligne[0], out id);
                DateTime deadline = Convert.ToDateTime(ligne[3]);
                if (success)
                {
                    Tache tmp = new Tache(id, ligne[1], ligne[2], deadline);
                    list_taches.Add(tmp);
                }
            }
            return list_taches;
        }

        public static void ecriture_fichier(List<Tache> list_taches, string path)
        {
            // Remise a 0 du fichier
            File.WriteAllText(path, "");

            foreach (Tache task in list_taches)
            {
                string tache_entiere = string.Format("{0};{1};{2};{3}", task.get_Id(), task.get_Name(), task.get_Desc(), task.get_Deadline());
                using (StreamWriter file = new StreamWriter(path, true))
                {
                    file.WriteLine(tache_entiere); // Add this line at the end of the text file
                }
            }
        }

        public static int Calcul_id(List<Tache> list_taches, int id) // fonction faite avec l'aide d'Arthur
        {
            bool restart;
            do
            {
                restart = false;
                foreach (Tache Tache_park in list_taches)
                {
                    if (id == Tache_park.get_Id())
                    {
                        id = id + 1;
                        restart = true;
                        break;
                    }
                }
            } while (restart);
            return id;
        }

        public static int compteur(List<Tache> list_taches)
        {
            int compteur= 0 ;

            foreach (Tache task in list_taches)
            {
                compteur += 1;
            }

            return compteur;

        }
    }

}

