﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TaskManagerLib
{
    public class Tache
    {
        
        protected int Id;
        protected string Name;
        protected string Description;
        protected DateTime Deadline;

        public Tache(int id, string name, string description, DateTime deadline)
        {
            Id = id;
            Name = name;
            Description = description;
            Deadline = deadline;
        }

        public int get_Id()
        {
            return Id;
        }

        public string get_Name()
        {
            return Name;
        }
        public string get_Desc()
        {
            return Description;
        }
        public DateTime get_Deadline()
        {
            return Deadline;
        }       

        public void set_Name(string Name)
        {
            this.Name = Name;
        }
        public void set_Desc(string Desc)
        {
            this.Description = Desc;
        }
        public void set_Deadline(DateTime Deadline)
        {
            this.Deadline = Deadline;
        }
      
    }
    
}
