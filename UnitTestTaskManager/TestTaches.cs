﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskManagerLib;

namespace UnitTestTaskManager
{
    [TestClass]
    public class TestTaches
    {
        [TestMethod]
        public void TestMethod1()
        {
        }
        public void Testgetid()
        {
            string[] tab = { "1", "1ere Tache", "description de la tache 1", "16/05/19"};
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            Assert.AreEqual(1, tache.get_Id());
        }

        [TestMethod]
        public void Testgetname()
        {
            string[] tab = { "1", "1ere Tache", "description de la tache", "29/05/19"};
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            Assert.AreEqual("1ere Tache", tache.get_Name());
        }

        [TestMethod]
        public void Testgetdesc()
        {
            string[] tab = { "1", "1ere Tache", "la 1ere description", "12 mai"};
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            Assert.AreEqual("la 1ere description", tache.get_Desc());
        }

        [TestMethod]
        public void Testgetdeadline()
        {
            string[] tab = { "1", "1ere Tache", "description de la tache", "29/05/19"};
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            Assert.AreEqual("29/05/2019 00:00:00", string.Format("{0}", tache.get_Deadline()));
        }

        [TestMethod]
        public void Testsetname()
        {
            string[] tab = { "1", "1ere Tache", "description de la tache", "02/05/19"};
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            tache.set_Name("Autre nom");
            Assert.AreEqual("Autre nom", tache.get_Name());
        }

        [TestMethod]
        public void Testsetdesc()
        {
            string[] tab = { "1", "1ere Tache", "description de la tache", "02/05/19" };
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            tache.set_Desc("Autre description");
            Assert.AreEqual("Autre description", tache.get_Desc());
        }

        [TestMethod]
        public void Testsetdeadline()
        {
            string[] tab = { "1", "1ere Tache", "description de la tache", "02/05/19" };
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            DateTime Deadlinetest = new DateTime(2021, 08, 14);
            tache.set_Deadline(Deadlinetest);
            Assert.AreEqual("14/08/2021 00:00:00", string.Format("{0}", tache.get_Deadline()));
        }
       
    }
}

