using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskManagerLib;
using System.Collections.Generic;
using BasicTaskManager;

namespace UnitTestTaskManager
{
    [TestClass]
    public class TestFichiers
    {
        [TestMethod]
        public void TestCompteur()
        {
            List<Tache> list_taches = new List<Tache>();
            string[] tab = { "1", "Correction du TD", "Mettez nous une bonne note s'il vous plait", "27/05/19" };
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            list_taches.Add(tache);
            string[] tab2 = { "2", "R�vision intensive", "Objectif bonne note", "28/05/19" };
            int a2 = int.Parse(tab2[0]);
            DateTime Deadline2 = Convert.ToDateTime(tab2[3]);
            Tache tache2 = new Tache(a2, tab2[1], tab2[2], Deadline2);
            list_taches.Add(tache2);
            Assert.AreEqual(2, Fichier.compteur(list_taches));

        }


        [TestMethod]
        public void TestEcritureFichier()
        {
            string path = @"AppelAuDon.txt";
            Fichier.ouverture_fichier(path);
            List<Tache> list_taches = new List<Tache>();
            string[] tab = { "1", "Correction du TD", "Mettez nous une bonne note s'il vous plait", "27/05/19" };
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            list_taches.Add(tache);
            Fichier.ecriture_fichier(list_taches, path);
            Assert.AreEqual("1;Correction du TD;Mettez nous une bonne note s'il vous plait;27/05/2019 00:00:00\r\n", string.Format("1;Correction du TD;Mettez nous une bonne note s'il vous plait;27/05/2019 00:00:00\r\n"));
        }

        [TestMethod]
        public void TestCalculId()
        {
            List<Tache> list_taches = new List<Tache>();
            string[] tab = { "1", "Correction du TD", "Mettez nous une bonne note s'il vous plait", "27/05/19" };
            int a = int.Parse(tab[0]);
            DateTime Deadline = Convert.ToDateTime(tab[3]);
            Tache tache = new Tache(a, tab[1], tab[2], Deadline);
            list_taches.Add(tache);
            string[] tab2 = { "2", "R�vision intensive", "Objectif bonne note", "28/05/19"};
            int a2 = int.Parse(tab2[0]);
            DateTime Deadline2 = Convert.ToDateTime(tab2[3]);
            Tache tache2 = new Tache(a2, tab2[1], tab2[2], Deadline2);
            list_taches.Add(tache2);
            Assert.AreEqual(3, Fichier.Calcul_id(list_taches, 1));
        }



    }
}
